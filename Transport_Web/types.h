/*
 * types.h
 * 
 * Copyright 2020 Vas Sferd <vassferd@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef _TRANSPORT_WEB_TYPES_H
#define _TRANSPORT_WEB_TYPES_H

#include "dsgenerator.h"



typedef struct graph_ graph_t; // Сам Граф 
typedef struct vertex_ vertex_t; // Вершина и данные об исходящих дугах 
typedef struct arc_ arc_t; // 

typedef int vertex_name_t; // Имя вершины - целое число
typedef int bandwidth_t;

struct graph_
{	
	vertex_t * vertexes; // Все вершины
	vertex_name_t last; // Имя последней вершины
};

struct vertex_
{	
	DSG_LIST( arc_t) forwards; // Массив исходящих дуг
	
	vertex_name_t * sources; // Массив имён 
};

struct arc_
{
	vertex_name_t target; // Целевая вершина
	bandwidth_t max_bandwidth; // Максимальная пропускная способность
};

#endif
