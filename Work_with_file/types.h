/*
 * types.h
 * 
 * Copyright 2020 Vas Sferd <vassferd@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef _TYPES_H
#define _TYPES_H

enum
{
	 QUIT = 0,	// Выход из программы
	 GO			// Продолжен
};

typedef enum
{
	READ,		// Чтение данных
	CAT,		// Вывод данных
	EXECUTE,	// Выполнить расчёт
	ADD, 		// Добавить запись
	DELETE, 	// Удаление записи
	SAVE, 		// Cохранение файла
	STATUS,		// Вывод информации о записях
	HELP,		// ВЫвод списка комманд
	ERROR, 		// Ошибка
	EXIT		// Выход

} COMMAND;


typedef char * error_message_t;

typedef char * film_name_t;
typedef unsigned minute_t;

typedef struct
{
	film_name_t name;			// Имя фильма
	minute_t duration;			// Продолжиьельность (мин)

} record_t;

typedef void self_t;

typedef struct
{
    int index;					// Индекс элемента изначальной таблицы или маркер добавленного элемента
    record_t * added;			// Добавляемый узел
    
    self_t * next;				// Указатель на следдующий элемент

} patch_table_t;

typedef struct
{
	record_t * records;			// Указатель на массив записей
	int count; 					// Число записей
	
    patch_table_t * diff;		// Таблица изменеия
	int d_count;				// Изменение кол-ва элементов после пересчёта 
	
    FILE * file;				// Указатель на файл-источник

} record_table_t;


#endif
