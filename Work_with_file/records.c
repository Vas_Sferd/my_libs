/*
 * records.c
 * 
 * Copyright 2020 Vas Sferd <vassferd@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "records.h"

/** Обработчик комманд **/
COMMAND getcommand()
{	
	while (TRUE)
	{
		char command[STRING_SIZE];
		
		printf( "$: ");
		
		scanf( "%s", command);
	
		if ( ! strcmp( command, "read"))
			return READ;
		
		if ( ! strcmp( command, "cat"))
			return CAT;
		
		if ( ! strcmp( command, "execute"))
			return EXECUTE;
		
		if ( ! strcmp( command, "add"))
			return ADD;

		if ( ! strcmp( command, "delete"))
			return DELETE;

		if ( ! strcmp( command, "save"))
			return SAVE;

		if ( ! strcmp( command, "status"))
				return STATUS;

		if ( ! strcmp( command, "help"))
			return HELP;
	
		if ( ! strcmp( command, "error"))
			return ERROR;
	
		if ( ! strcmp( command, "exit"))
			return EXIT;

		perror( "Incorrect command\n");
	}
}

/** Вывод ошибки и остановка **/
void perr( const char * error_message) // Вывод ошибки и остановка программы
{
	perror( error_message);
	abort();
}

/** Закрытие файла **/
void tclose( record_table_t * Table)
{
	if ( Table == NULL)						// Проверка на существование таблицы
		return;
	
	fclose( Table->file);
	
	for ( int i = 0; i < Table->count; ++i)
	{
		free( Table->records[i].name);		// Освобождениее памяти имён
	}
	
	free( Table->records);					// Освобождение таблицы элементов
	
	patch_table_t * diff = Table->diff;
	
	while ( diff != NULL)
	{
		if ( diff->index == MARKER_ADDED)
		{
			free( diff->added->name);		// Освобождение памяти имён добавлемых элементов
			free( diff->added);				// Освобождение памяти добавляемых элементов
		}
		
		patch_table_t * tmp = diff;
		diff = diff->next;
			
		free( tmp);
	}
	
	free( Table);
		
	return;
}

/** Проверка на существование таблицы **/
void checktable( record_table_t ** ptr_Table)
{
	record_table_t * Table = * ptr_Table;
	
	if ( Table == NULL)											
	{
		perror( "You must first read the file!\n");
		printf( "$: read ");
		
		Table = readfile(); 				// Считываем данные из файла в память
		* ptr_Table = Table; 				// Сохраняем новое значение таблицы
	}
	
	return;
}

/** Команды **/

/* READ */
record_table_t * readfile( void)				// Получение данных из файла
{
	while (TRUE)
	{
		record_table_t * Table = (record_table_t *)malloc( sizeof( record_table_t));
		
		Table->file = selectfile( "r+");		// Выбираем новый файл
		
		Table->count = fcountr( Table);			// Подсчёт числа записей в таблицу
		fseek( Table->file, 0, SEEK_SET);		// Возвращаемся в начало файла
		readrecords( Table);					// Чтение записей
		
		Table->diff = NULL;
		Table->d_count = 0;

		if ( Table->file != NULL)
		{
			printf( "Successful reading (%d elements)\n", Table->count);
			return Table;
		}
	}
}

inline static const char * getfilename( void)	// Получаем имя файла
{	
	char * filename = (char *)malloc( STRING_SIZE * sizeof(char)); // Имя файла
	scanf( "%s", filename);
	
	return filename;
}

FILE * selectfile( const char * mode) 			// Выбор файла
{
	while ( TRUE)
	{
		const char * filename = getfilename();	// Получение имени
		
		FILE * file = fopen( filename, mode);
		
		fflush( stdin);							// Очистка потка ввода
		
		if ( file != NULL)						// Удачное открытие файла
		{
			return file;
		}
	
		printf( "Unable to open this file or file not found\nTry again\n");
		printf( "$: read ");
	}
}

int fcountr( record_table_t * Table) // Подсчёт числа записей
{
	char sym;				// Символ
	int record_count = 0;	// Счётчик записей 
	
	while ( ( sym = getc( Table->file)) != EOF)
		if ( sym == '#')
			++record_count;
	
	return record_count;
}

void readrecords( record_table_t * Table) // Считывание записей
{	
	const char END_OF_NAME[] = "\" : ";	// Разделитель между именем и продолжительностью 
	
	Table->records = (record_t *)malloc( Table->count * sizeof( record_t));
	// Выделение памяти под записи
	
	for ( int i = 0; i < Table->count; ++i)
	{
		Table->records[i].name = (char *)malloc( STRING_SIZE * sizeof(char));
		// Выделяем память под названия фильмов
		
		char sym;			// Символ 
		int str_pose = 0;	// Позиция добовляемого символа в строке 
		int eon_equal = 0;	// Число последовательных совпадений с END_OF_NAME
		
		// while ( getc( Table->file) != '#');
		
		fscanf( Table->file, "\n# \"");	// Пропускаем начало записи
		
		while ( eon_equal < 4)
		{
			sym = getc( Table->file);
			
			if ( str_pose >= STRING_SIZE)
				perr( "FATAL ERROR: Incorrect file format or insufficient name memory\n");
			
			if ( sym == END_OF_NAME[eon_equal])	// Проверка на стоп последовательность
			{
				++eon_equal;
				
				continue;						// Переход к следуещему символу
			}
			else
			{
				if ( eon_equal != 0)						// Проверка на разрыв стоп последовательности
				{
					for ( int j = 0; j < eon_equal; ++j)	// Возвращаем символы в имя
					{
						Table->records[i].name[str_pose] = END_OF_NAME[j]; 
					
						++str_pose;
					}
			
					eon_equal = 0;							// Обнуляем счётчик
				}
			
				Table->records[i].name[str_pose] = sym;		// Добавление буквы в имя
					
				++str_pose;
			}
		}
		
		Table->records[i].name[str_pose] = '\0';
		
		Table->records[i].duration = 0;
		
		fscanf( Table->file, "%d", &Table->records[i].duration);
	}

	return;
}

/* CAT */
void rprintf( record_table_t * Table) // Вывод записей
{
	printf( "Список фильмов: \n");
	printf( "№ Название \t Продолжительность(мин)\n");
	char name[STRING_SIZE + 2];

	for ( int i = 0; i < Table->count; ++i)
	{
		snprintf( name, sizeof( name),"%c%s%c", '"', Table->records[i].name, '"');
		
		printf( "%-2.0d : % s : %4u\n", ( i + 1), name, Table->records[i].duration);
	}

	return;
}

int maxduration( record_table_t * Table) // Ищем максимальную продолжительность фильма
{
	minute_t max = 0;

	for ( int i = 0; i < Table->count; ++i)
		if ( Table->records[i].duration > max)
			max = Table->records[i].duration;

	return max;
}

/* Работа с патчами */
void tgenpatch( record_table_t * Table) // Генерирует временную таблицу
{
	if ( Table->diff != NULL) // Проверка на непустоту
		return;
	
	patch_table_t * diff = (patch_table_t *)malloc( sizeof( patch_table_t));
	
	Table->diff = diff;
	diff->index = 0; // Нулевой элемент
	
	for ( int i = 1; i < Table->count; ++i)
	{
		diff->next = (patch_table_t *)malloc( sizeof( patch_table_t)); // Cледующий элемент
		
		diff = diff->next;
		diff->index = i;
	}
	
	diff->next = NULL; // Конец 
	
	return;
}

static void tdpaste( record_table_t * Table, patch_table_t * added, int index) // Вставляем новый элемент во временную таблицу
{
	patch_table_t * target = Table->diff; // Начинаем с позиции 1
	
	if ( index == 1)
	{
		added->next = Table->diff;
		Table->diff = added;
	}
	
	else
	{
		for (int i = 2; i < index; ++i) // Перемещаемся вперёд
		{
			target = target->next;
		}
		
		added->next = target->next;
		target->next = added;
	}
	
	return;
}

void trebuild( record_table_t * Table) // Пересборка таблицы
{
	patch_table_t * diff = Table->diff;
	
	if ( diff != NULL)
	{
		int n = Table->count + Table->d_count;									// Новый размер таблицы
	
		record_t * new_records = (record_t *)malloc( n * sizeof( record_t));	// Выделение памяти для нового массива записей
	
		for ( int i = 0; i < n; ++i)											// Пересборка таблицы
		{
			if ( diff->index == MARKER_ADDED)									// Перемещение новых данных
			{
				new_records[i].name = diff->added->name;
				new_records[i].duration = diff->added->duration;
			}
			else																// Перемещение старых данных
			{
				new_records[i].name = Table->records[diff->index].name;
				new_records[i].duration = Table->records[diff->index].duration;
			}
			
			patch_table_t * tmp = diff;
			diff = diff->next;
			
			free( tmp);
		}
	
		free( Table->records);			// Очищаем старую память
		
		Table->records = new_records;	// Обновляем ссылку на массив
		Table->count = n;				// Обновляем данные о колличестве элементов
	
		Table->diff = NULL;		// Сбрасываем указатель на временную таблицу
		Table->d_count = 0;		// Сбрасываем число добавленных элементов
	}
	
	return;
}
 
/* ADD */
char * scanname( void)	// Сканируем строку заключённую в кавычки
{
	char * name = (char *)malloc( STRING_SIZE * sizeof(char));
	
	char sym;			// Символ
	int i = 0;			// Счётчик
	
	if ( ( sym = getchar()) != '"')
	{
		perr( "FATAL ERROR: Incorrect name format !!!\nYou must use quotes\"");
	}
	
	while ( ( sym = getchar()) != '"')
	{
		name[i] = sym;
		
		++i;
		
		if ( i >= STRING_SIZE)
		{
			perr( "FATAL ERROR: Name too long");
		}
	}

	return name;
}


void taddr( record_table_t * Table) // Добавление записи в таблицу
{	
	tgenpatch( Table);
	
	int n = Table->count + Table->d_count;										// Текущее число записей
	
	int index;
	scanf( "%d", &index);

	if ( index > n + 1 || index <= 0)											// Проверка на выход за пределы массива
	{
		perror( "Error: Program can`t delete record. Incorrect index !!!\n");
		
		return;
	}

	patch_table_t * diff = (patch_table_t *)malloc( sizeof( patch_table_t));	// Патч
	diff->added = (record_t *)malloc( sizeof(record_t));						// Содержимое патча
	diff->index = MARKER_ADDED;													// Маркер добавленной записи

	record_t * added_record = diff->added;
	
	scanf( " ");						// Прорускаем лишний пробел
	added_record->name = scanname();	// Считываем название добавляемого элемента
	
	scanf( " %d", &added_record->duration);

	fflush( stdin);
	tdpaste( Table, diff, index);	// Вставляем элемент в предварительную таблицу

	Table->d_count += 1;

	return;
}

/* DELL */
void tdelr( record_table_t * Table)	// Удаление записи из таблицы
{
	tgenpatch( Table);
	
	int n = Table->count + Table->d_count;	// Текущее число записей
	patch_table_t * diff = Table->diff;		// Запись из временной таблицы

	int index;
	scanf( "%d", &index);
	
	if ( index >= n)						// Проверка на выход за пределы массива
	{
		perror( "Error: Program can`t delete record. Incorrect index !!!\n");
		
		return;
	}
	
	if ( index == 1)
	{		
		Table->diff = diff->next;				// Переводим начало на вторую позицию
	}
	else
	{
		for ( int i = 1; i < index - 1; ++i)	// Переходим к необходимому элементу
		{
			diff = diff->next;
		}
	
		patch_table_t * prev_diff = diff;		// Предыдущий элемент временной таблицы
		diff = diff->next;						// Удаляемый элемент временной таблицы
		
		prev_diff->next = diff->next;			// Пропускаем элемент diff во временной таблице
	}
	
	record_t * removed_record;				// Удаляемая запись
	index = diff->index;					// Настоящий индекс записи или маркер добавляемой записи 
	
	if ( index == MARKER_ADDED)
	{
		removed_record = diff->added;
	}
	else
	{
		removed_record = &Table->records[index];
	}
	
	printf( "Record [ \"%s\" : %d ] has been removed\n", removed_record->name, removed_record->duration);

	free( removed_record->name);	// Освобождаем память имени элемента
	
	if ( index == MARKER_ADDED)		// Для добавленных даныых
		free( removed_record);		// освобождаем память записи
	
	free( diff);					// Освобождаем память элемента временной таблицы

	fflush( stdin);
	Table->d_count -= 1;

	return;
}

/* SAVE */
void savetable( record_table_t * Table)	// Сохранение текущих данных в файл
{
	// printf("Введите имя файла для сохранения (существующие файлы будут перезаписаны):\n");

	fclose( Table->file);						// Закрываем старый файл без изменений
	Table->file = selectfile( "w");				// Выбираем новый файл для перезаписи
	
	for ( int i = 0; i < Table->count; ++i)		// Переапись данных
	{
		fprintf( Table->file, "# \"%s\" : %d\n", Table->records[i].name, Table->records[i].duration);
	}

	fflush( Table->file);

	return;
}

/* Status */
void printStatus( record_table_t * Table)	// Состояние базы данных
{
	const char * table_status =
		"На данный момент в таблице находится %d записей\n"
		"Послее пересборки таблицы число элементов изменится на %d\n"
		"Вы также можете использовать команду \"cat\",\n"
		"чтобы пересобрать таблицу и вывести необхоимые значения\n";
	
	printf( table_status, Table->count, Table->d_count);
	
	return;
}

/* Help */
void phelp( void)	// Печать списка доступных комманд
{
	const char * help_list =
		"Доступные команды:\n"
		"* read <имя файла> - Считывает данные из памяти в буфер (Предыдущий файл закрывается)\n"
		"* cat - Выводит данные из буфера\n"
		"* execute - Поиск максимально продолжительного фильма\n"
		"* add <индекс> \"<название>\" <длительность> - Добавляет запись\n"
		"* delete <индекс> - Удаляет запись по индексу\n"
		"* save <имя файла> - Сохранение буфера в указанный файл\n"
		"* status - Состояние программы\n"
		"* help - список команд\n"
		"* error - тестовая ошибка\n"
		"* exit - выход\n";
	
	printf( help_list);
	
	return;
}
