/*
 * records.h
 * 
 * Copyright 2020 Vas Sferd <vassferd@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef _RECORDS_H
#define _RECORDS_H

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <locale.h>
#include <string.h>
#include "types.h"

#define STRING_SIZE 120

/** Обработчик комманд **/
COMMAND getcommand();

/** Ошибка **/
void perr( const char * error_message);

/** Закрытие файла **/
void tclose( record_table_t * Table);

/** Проверка на существование таблицы **/
void checktable( record_table_t * Table);

/** Команды **/

/* READ */
record_table_t * readfile( void);			// Получение данных из файла
FILE * selectfile(); 						// Выбор файла
int fcountr( record_table_t * Table);		// Подсчёт числа записей
void readrecords( record_table_t * Table);	// Считывание записей

/* CAT */
void rprintf( record_table_t * Table); 		// Вывод записей

/* EXECUTE */
int maxduration( record_table_t * Table);	// Ищем максимальную продолжительность фильма

/* Работа с патчами */
void tgenpatch( record_table_t * Table);	// Генерирует временную таблицу
void trebuild( record_table_t * Table);		// Пересборка таблицы

/* ADD */
void taddr( record_table_t * Table);		// Добавление записи в таблицу
#define MARKER_ADDED -1						// Маркер добавленного элемента

/* DELL */
void tdelr( record_table_t * Table);		// Удаление записи из таблицы

/* SAVE */
void savetable( record_table_t * Table);	// Сохранение текущих данных в файл

/* Status */
void printStatus( record_table_t * Table); 	// Состояние базы данных

/* Help */
void phelp( void);							// Печать списка доступных комманд


#endif
