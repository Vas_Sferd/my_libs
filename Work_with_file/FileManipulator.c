/*
 * FileManipulator.c
 * 
 * Copyright 2020 Vas Sferd <vassferd@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <stdio.h>

#include "types.h"
#include "records.h"


// Variant 20
// Фильмы и их продолжительность

int menu( record_table_t ** ptr_Films)
{
	record_table_t * Films = * ptr_Films;		// Получаем значение
	
	switch ( getcommand())						// Получаем команду из потока ввода
	{
		case READ :
			tclose( Films); 					// Закрываем файл и очищаем данные в памяти
			
			Films = readfile(); 				// Считываем данные из файла в память
			* ptr_Films = Films; 				// Сохраняем новое значение таблицы
			return GO;
		
		case CAT :
			checktable( ptr_Films);				// Проверка существования таблицы
			
			trebuild( Films);					// Пересборка таблицы
			rprintf( Films);					// Вывод записей
			return GO;
	
		case EXECUTE :
			checktable( ptr_Films);				// Проверка существования таблицы
			
			trebuild( Films);					// Пересборка таблицы
			minute_t max = maxduration( Films); // Поиск максимума в таблице

			printf( "Maximum of film`s duration = %d\n", max);
			return GO;
	
		case ADD :
			checktable( ptr_Films);				// Проверка существования таблицы
			
			taddr( Films);						// Добавление фильма в таблицу
			return GO;
	
		case DELETE :
			checktable( ptr_Films);				// Проверка существования таблицы
			
			tdelr( Films);						// Удаление фильма из таблицы	
			return GO;
	
		case SAVE :
			checktable( ptr_Films);				// Проверка существования таблицы
			
			trebuild( Films);					// Пересборка таблицы
			savetable( Films);					// Запись данных таблицы в изначальный файл 
			return GO;

		case STATUS :
			checktable( ptr_Films);				// Проверка существования таблицы
		
			printStatus( Films);				// Печать состояния
			return GO;

		case HELP :
			phelp();							// Помощь
			return GO;

		case ERROR :
			perr( "Test error: Program must been stopped\n");
			return QUIT;
	
		case EXIT :
			tclose( Films); 				// закрываем файл и очищаем данные в памяти
			return QUIT;
	}

}

int main(void)
{	
	setlocale(LC_ALL, "RU");		// Локализация

	printf( "It is FileManipulator.\nWrite \"help\" if you wanna known command list.\n");
	
	record_table_t * Films = NULL;	// Ссылка на таблицу со значениями
	
	while ( menu( &Films));			// Цикл программы
	
	return 0;
}
