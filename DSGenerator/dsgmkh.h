/*
 * dsgmkh.h
 * 
 * Copyright 2020 Vas Sferd <vassferd@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

/**************************************
 * Use the file if you create pattern
 * This file used with
 * "Data Structs Generator"
 * (by Vas Sferd)
 *
 * Using DSG const:
 * DSG_DATA_TYPE // Тип ваших данных 
 * DSG_STRUCT_TYPE // Тип структуры
**************************************/ 
 
#ifndef _DSG_DSGMKH_H
#define _DSG_DSGMKH_H

/* Обшие макросы */

/* Разворачивание макроса */
#define EX( CONST) \
		EX__( CONST)
#define EX__( CONST) \
		CONST

#define CSTR( CONST) #CONST // Конвертация константы в строку

// Соединяем строки
#define CONCAT( FIRST, SECOND) \
		CONCAT__( FIRST, SECOND) 
#define CONCAT__( FIRST, SECOND) \
		FIRST ## SECOND


/* Специальные макросы */

/* Генерация константы заголовка для защит от множественношо присоединения типов */
#define DSG_HEADER( DSG_STRUCT_TYPE, DSG_DATA_TYPE) \
		DSG_HEADER__( DSG_STRUCT_TYPE, DSG_DATA_TYPE)  
#define DSG_HEADER__( DSG_STRUCT_TYPE, DSG_DATA_TYPE) \
		_ ## DSG_STRUCT_TYPE ## _OF_ ## DSG_DATA_TYPE ## _H
		
/* Создание имени нового DSG-типа */
#define DSG_MK_TYPE( DSG_STRUCT_TYPE, DSG_DATA_TYPE) \
		DSG_MK_TYPE__( DSG_STRUCT_TYPE, DSG_DATA_TYPE)
#define DSG_MK_TYPE__( DSG_STRUCT_TYPE, DSG_DATA_TYPE) \
		DSG_STRUCT_TYPE ## _of_ ## DSG_DATA_TYPE

/* Создание имени функции */
#define DSG_MK_FUNC( DSG_DATA_TYPE, DSG_FUNC_NAME) \
		DSG_MK_FUNC__( DSG_DATA_TYPE, DSG_FUNC_NAME)
#define DSG_MK_FUNC__( DSG_DATA_TYPE, DSG_FUNC_NAME) \
		DSG_DATA_TYPE ## _ ## DSG_FUNC_NAME



#endif
