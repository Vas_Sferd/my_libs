/*
 * list.h
 * 
 * Copyright 2020 Vas Sferd <vassferd@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

/**************************************
 * !!! Do not include !!!
 * This file used with
 * "Data Structs Generator"
 * (by Vas Sferd)
 *
 * Using DSG constant:
 * > DSG_DATA_TYPE // Тип ваших данных
 * > DSG_DATA_CMP // Функция для сравнения ваших данных
 * > DSG_DATA_FREE // Функция для очистки данных данного формата
 * > DSG_DATA_NAME // Имя поля для ваших данных
 * * DSG_STRUCT_TYPE // Тип структуры
**************************************/
 
#include "dsgmkh.h" // Используем файл "list.h" как DSG паттерн

#define DSG_STRUCT_TYPE DSG_LIST // Обозначаем тип структуры для конструктора

/* DSG_DATA_TYPE-зависимая не универсальная часть */
#ifndef DSG_HEADER( DSG_STRUCT_TYPE, DSG_DATA_TYPE)
#define DSG_HEADER( DSG_STRUCT_TYPE, DSG_DATA_TYPE)

	#define DSG_LIST_T DSG_MK_TYPE( list, DSG_DATA_TYPE)
	#define DSG_LIST_NODE_T DSG_MK_TYPE( list_node, DSG_DATA_TYPE)
	// Объявляем временные маски для имени новых типов

	typedef struct
	{
		int count; // Число элементов в листе 
		
		DSG_LIST_NODE_T * first; // Ссылка на первый элемент
		DSG_LIST_NODE_T * last; // Ссылка на второй элемент
	} DSG_LIST_T;

	typedef struct
	{
		DSG_DATA_TYPE * DSG_DATA_NAME; // Поле с вашими данными
	
		DSG_LIST_NODE_T prev; // Ссылка на предыдущий элемент
		DSG_LIST_NODE_T next; // Ссылка на следующий элемент
	} DSG_LIST_NODE_T;
	
	
	/* Универсальная часть ( Объявления функций и основных макросов) */
	#ifndef LIST_H
	#define LIST_H	
	
		/* Выделение памяти и создание структур */
		DSG_LIST_T * mkList(); // Создание листа
		DSG_LIST_NODE_T * mkListNode( DSG_DATA_TYPE * DSG_DATA_NAME); // Создание узла листа
		
		/* Связывание узлов */
		inline void lnlink( DSG_LIST_NODE_T * first_ListNode, DSG_LIST_NODE_T * second_ListNode); // Создаёт связь first->second
		
		/* Перемещение по списку и получение данных по порядковому номеру */
		DSG_LIST_NODE_T * lgoto( DSG_LIST_T * List, int position); // Возвращает адрес узла по порядковому номеру
		
		/* Добавление элементов */
		void laddnode( DSG_LIST_T * List, DSG_LIST_NODE_T * added_ListNode, DSG_LIST_NODE_T * target_ListNode); // Добавление узла в лист после указанного
		void lappend( DSG_LIST_T * List, DSG_DATA_TYPE * added_Data); // Добавление элемента в конец
		int lextend( DSG_LIST_T * List, DSG_LIST_T * added_List); // Добавление листа в конец другого листа (Вставляемый лист не сохраняется) Возвращает число добавленных элементов. 
		inline void linsert( DSG_LIST_T * List, int position, DSG_LIST_NODE_T * added_ListNode); // Добавление следующим после позиции
		
		/* Поиск */
		int lfindg( DSG_LIST_T * List, DSG_DATA_TYPE * value, int start_pos, int end_pos); // Первое нахождениедение на промежутке
		int lfind( DSG_LIST_T * List, DSG_DATA_TYPE * value); // Первое нахождение во всём списке
		
		/* Удаление из структуры ( Память, выделяемая под данные, не освобождается !!) */
		void lrm( DSG_LIST_T * List, DSG_LIST_NODE_T * removed_ListNode); // Удаляет узел из листа
		void lremove( DSG_LIST_T * List, DSG_DATA_TYPE * removeable_);	// Удаляет первый элемент списка с заданным значением
		DSG_DATA_TYPE * lpop( DSG_LIST_T * List, int position); // Удаляет элемент из листа и возвращает адрес на него
		DSG_DATA_TYPE * lpopl( DSG_LIST_T * List); // Удаляет последний элемент из листа и возвращает адрес на него
		
		/* Удаление частей структуры ( Память, выделяемая под данные, освобождается !!) */
		void lfree( DSG_LIST_T * List); // Освобождает память, выделяемую под список и все включаемые данные
		void lclear( DSG_LIST_T * List); // Освобождает память всех включенных в список узлов и память включаемых данных 
		void lnfree( DSG_LIST_T * List, DSG_LIST_NODE_T * removed_ListNode); // Осаобождается память для данного узла и всех включённых в него данных
		
		/* Переход по списку */
		
		/* Переход вперёд */
		#define LBACK( CURRENT_ListNode) (CURRENT_ListNode != NULL)
			( ( ( CURRENT_ListNode) != NULL) ? CURRENT_ListNode->prev : NULL)

		/* Переход назад */
		#define LNEXT( CURRENT_ListNode) \
			( ( ( CURRENT_ListNode) != NULL) ? CURRENT_ListNode->next : NULL) 
			
		
	#else
		
		/* Не универсальная часть - обёртки функций */
		
		/* Функции выделения памяти */
		inline DSG_LIST_T * DSG_MK_FUNC( DSG_DATA_TYPE, mkList)()
		{
			return (DSG_LIST_T *) mkList();
		}
		
		inline DSG_LIST_NODE_T * DSG_MK_FUNC( DSG_DATA_TYPE, mkListNode)( DSG_DATA_TYPE * DSG_DATA_NAME) 
		{
			return (DSG_LIST_NODE_T *) mkListNode( DSG_DATA_TYPE * DSG_DATA_NAME);
		}
		
		/* Функции изъятия данных*/
		inline DSG_DATA_TYPE * DSG_MK_FUNC( DSG_DATA_TYPE, lpop)( DSG_LIST_T * List, int position)
		{
			return (DSG_DATA_TYPE *)lpop( List, position);
		}
		
		inline DSG_DATA_TYPE * DSG_MK_FUNC( DSG_DATA_TYPE, lpopl)( DSG_LIST_T * List, int position)
		{
			return (DSG_DATA_TYPE *)lpopl( List, position);
		}
		
	#endif
	
	/* Очистка констант */
	#udef DSG_LIST_T
	#udef DSG_LIST_NODE_T

#endif

/* Очистка входных констант */
#udef DSG_DATA_TYPE
#udef DSG_DATA_FREE
#udef DSG_DATA_NAME
#udef DSG_STRUCT_TYPE
