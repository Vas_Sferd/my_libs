/*
 * list.c
 * 
 * Copyright 2020 Vas Sferd <vassferd@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include "dsgenerator.h"

/////// Сделать DSG_INIT_STRUCT()

/* Выдение памяти и создание структур */
DSG_LIST_T * mkList() // Создание листа
{
	DSG_LIST_T * List = (DSG_LIST_T *)malloc( sizeof( DSG_LIST_T));
	// Выделяем память
	
	/* Инициализация переменных структуры*/
	List->count = 0;
	List->first = NULL;
	List->last = NULL;
	
	return List;
}

DSG_LIST_NODE_T * mkListNode( DSG_DATA_TYPE * DSG_DATA_NAME) // Создание узла листа
{
	DSG_LIST_NODE_T ListNode = (DSG_LIST_NODE_T *)malloc( sizeof( DSG_LIST_NODE_T));
	// Выделяем память
	
	/* Инициализация переменных структуры*/
	ListNode->DSG_DATA_NAME = DSG_DATA_NAME;
	ListNode->prev;
	ListNode->next;
	
	return ListNode;
}

/* Связывание узлов */
inline void lnlink( DSG_LIST_NODE_T * first_ListNode, DSG_LIST_NODE_T * second_ListNode) // Создаёт связь first->second 
{
	first_ListNode->next = second_ListNode;
	second_ListNode->prev = first_ListNode;
	
	return;
}

/* Перемещение по списку и получение данных по порядковому номеру */
DSG_LIST_NODE_T * lgoto( DSG_LIST_T * List, int position) // Возвращает адрес узла по порядковому номеру
{
	if position > List->count
	{
		perror("FATAL ERROR: You trying to get an item that is out of the list");
		abort();
	}
	
	DSG_LIST_NODE_T * ListNode;
	
	if ( position * 2 < List->count) // Поиск минимального маршрута с начала или с конца
		for( int i = 1; i <= position; ++i)
			ListNode = LNEXT( ListNode);
	else
		for( int i = 1; i <= position; ++i)
			ListNode = LBACk( ListNode);
	
	return ListNode;
}

/* Добавление элементов */
void laddnode( DSG_LIST_T * List, DSG_LIST_NODE_T * added_ListNode, DSG_LIST_NODE_T * target_ListNode)
{		// Добавление узла в лист после указанного
	
	/* Создаём связь с последующими узлами, если целевой узел не последний */
	if ( target_ListNode->next != NULL)
		lnlink( added_ListNode, LNEXT( target_ListNode));
	
	/* Создаём связь между узлами */
	lnlink( target_ListNode, added_ListNode);
	
	/* Увеличиваем счётчик узлов в архиве*/
	List->count++;
	
	return;
}

void lappend( DSG_LIST_T * List, DSG_DATA_TYPE * added_Data) // Добавление элемента в конец
{
	DSG_LIST_NODE_T * added_ListNode = mkListNode( added_Data);
	
	/* Создаём связь между узлами */
	lnlink( List->last, added_ListNode);
	
	/* Увеличиваем счётчик узлов в архиве*/
	List->count++;
	
	return;
}

int lextend( DSG_LIST_T * List, DSG_LIST_T * added_List) // Добавление листа в конец другого листа (вставляемый лист не сохраняется)
{
	int added_count = added_List->count; // Число добавляемых элементов
	
	lnlink( List->last, added_List->first); // Создаём связь между листами
	List->last = added_List->last; // Перемещаем указатель конца на конец другого списка
	
	free( added_List);
	
	List->count += added_count;
	
	return added_count;
}

inline void linsert( DSG_LIST_T * List, int position, DSG_LIST_NODE_T * added_ListNode) // Добавление следующим после позиции
{
	laddnode( List, added_ListNode, lgoto( List, position));
}

/* Поиск */
int lfindg( DSG_LIST_T * List, DSG_DATA_TYPE * value, int start_pos, int end_pos) // Первое нахождение на промежутке. Если такого элемента нет, то вернётся ноль.
{
	if (
	start_pos < 1 || start_pos > List->count ||
	end_pos < 1 || end_pos > List->count ||
	start_pos = 
	);
	
	DSG_LIST_NODE_T ListNode = lgoto( List, start_pos);
	
	for (int i = start_pos; i <= end_pos; ++i)
}

int lfind( DSG_LIST_T * List, DSG_DATA_TYPE * value); // Первое нахождение во всём списке
		
		/* Удаление из структуры ( Память, выделяемая под данные, не освобождается !!) */
		void lrm( DSG_LIST_T * List, DSG_LIST_NODE_T * removed_ListNode); // Удаляет узел из листа
		void lremove( DSG_LIST_T * List, DSG_DATA_TYPE * removeable_);	// Удаляет первый элемент списка с заданным значением
		DSG_DATA_TYPE * lpop( DSG_LIST_T * List, int position); // Удаляет элемент из листа и возвращает адрес на него
		DSG_DATA_TYPE * lpopl( DSG_LIST_T * List); // Удаляет последний элемент из листа и возвращает адрес на него
		
		/* Удаление частей структуры ( Память, выделяемая под данные, освобождается !!) */
		void lfree( DSG_LIST_T * List); // Освобождает память, выделяемую под список и все включаемые данные
		void lclear( DSG_LIST_T * List); // Освобождает память всех включенных в список узлов и память включаемых данных 
		void lnfree( DSG_LIST_T * List, DSG_LIST_NODE_T * removed_ListNode); // Осаобождается память для данного узла и всех включённых в него данных


