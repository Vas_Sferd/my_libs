/*
 * block_list.h
 * 
 * Copyright 2020 Vas Sferd <vassferd@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#ifndef _BLOCK_LIST_H
#define _BLOCK_LIST_H

#define define_BlocListType(__NEW_TYPE, __BLOCK_SIZE, __ELEM_TYPE, __ELEM_PRINT, __ELEM_FREE, __ELEM_CMP)		\
																												\
typedef struct __NEW_TYPE ## _struct																			\
{																												\
	/*//	HEADER		//*/																					\
	struct blist_struct * prev;																					\
	struct blist_struct * next;																					\
	const size_t _count;																						\
	const size_t _elem_size;																					\
																												\
	/*//	DATA		//*/																					\
	__ELEM_TYPE elems[__BLOCK_SIZE];																			\
																												\
} __NEW_TYPE;																									\
																												\
inline __NEW_TYPE * mk ## __NEW_TYPE 			();																\
inline void 		__NEW_TYPE ## _pushback		(__NEW_TYPE * blist, __ELEM_TYPE elem);							\
inline void 		__NEW_TYPE ## _pushfront	(__NEW_TYPE * blist, __ELEM_TYPE elem);							\
inline void 		__NEW_TYPE ## _insert		(__NEW_TYPE * blist, __ELEM_TYPE elem, int position);			\
inline void 		__NEW_TYPE ## _remove		(__NEW_TYPE * blist, int position);								\
inline void 		__NEW_TYPE ## _set			(__NEW_TYPE * blist, __ELEM_TYPE elem, int position);			\
inline void 		__NEW_TYPE ## _clean		(__NEW_TYPE * blist);											\
inline int			__NEW_TYPE ## _find			(__NEW_TYPE * blist, __ELEM_TYPE elem);							\
inline void			__NEW_TYPE ## _print		(__NEW_TYPE * blist);											\
																												\
																												\
inline __NEW_TYPE *	mk ## __NEW_TYPE 			()																\
{/*****************/return (__NEW_TYPE)			_mkBlockList(__BLOCK_SIZE, sizeof(__ELEM_TYPE))}				\													\														\
																												\
inline void 		__NEW_TYPE ## _pushback		(__NEW_TYPE * blist, __ELEM_TYPE elem)							\
{/*****************/_blockList_pushback			(blist, &elem);	return;}										\
																												\
inline void 		__NEW_TYPE ## _pushfront	(__NEW_TYPE * blist, __ELEM_TYPE elem)							\
{/*****************/_blockList_pushfront		(blist, &elem);	return;}										\
																												\
inline void 		__NEW_TYPE ## _insert		(__NEW_TYPE * blist, __ELEM_TYPE elem, int position)			\
{/*****************/_blockList_insert			(blist, &elem, position);	return;}							\
																												\
inline void 		__NEW_TYPE ## _remove		(__NEW_TYPE * blist, int position)								\
{/*****************/_blockList_remove			(blist, position);	return;}									\
																												\
inline void 		__NEW_TYPE ## _set			(__NEW_TYPE * blist, __ELEM_TYPE elem, int position);			\
{/*****************/_blockList_set				(blist, elem, position);	return;}							\
																												\
inline void 		__NEW_TYPE ## _clean		(__NEW_TYPE * blist);											\
{/*****************/_blockList_clean			(blist, __ELEM_FREE);	return;}								\
																												\
inline int			__NEW_TYPE ## _find			(__NEW_TYPE * blist, __ELEM_TYPE elem)							\
{/*****************/return	_blockList_find		(blist, elem, __ELEM_CMP);	return;}							\
																												\
inline void			__NEW_TYPE ## _print		(__NEW_TYPE * blist)											\
{/*****************/_blockList_print			(blist, __ELEM_PRINT);	return;}								\																			\
																												\
#ifdef				__DSG_DEBUG__																				\
void				__NEW_TYPE ## _secretPrint	(__NEW_TYPE * blist)											\
{/*****************/_blockList_secretprint		(blist, __ELEM_PRINT);	return;}								\																												\
#endif																											\
																												\
																												\
																												\
#endif
