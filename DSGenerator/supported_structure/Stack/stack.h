/*
 * stack.h
 * 
 * Copyright 2020 Vas Sferd <vassferd@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#ifndef STACK_C
#define STACK_C

#include <stdio.h>

/* Configuration */
#define _STACK_DATA int // Type of the stack element
// #define _COMPLEX_STACK_DATA
/* Use it if _STACK_DATA is complex type
 * WARNING!!! YOU must create function:
 * "Stack_Node_free(_STACK_DATA Data)"
 * */
/*****************/


/* Structures */
#define STACK_NODE struct stack_node_*
#define STACK_NODE_STRUCT_SIZE (sizeof(stack_node_))
struct stack_node_
{
	_STACK_DATA data;
	
	struct stack_node_* next;
}

#define STACK struct stack_*
#define STACK_STRUCT_SIZE (sizeof(struct stack_))
struct stack_
{
	struct stack_node_* current;

	unsigned size; // count of element
};
/**************/


/* Func-like Macros */
#define Stack_top(Stack) (Stack->current->data)
// Return top value (_STACK_DATA)

#define Stack_isEmpty(Stack) (!Stack->size)
// Return 1 if Stack are empty, else return 0;
/********************/


/* Fuction Declaration */

extern STACK new_Stack();
extern void Stack_push(STACK Stack, _STACK_DATA data);
extern _STACK_DATA Stack_pop(STACK Stack);
extern void Stack_makeNull(STACK Stack);


#ifdef _COMPLEX_STACK_DATA		
extern void Stack_Node_free(_STACK_DATA Data);
#endif

#endif
