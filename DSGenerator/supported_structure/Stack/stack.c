/*
 * stack.c
 * 
 * Copyright 2020 Vas Sferd <vassferd@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <stack.h>
#include <malloc.h>
#include <stdio.h>


static STACK_NODE new_Stack_Node(_STACK_DATA data)  
{
	STACK_NODE Stack_Node = (STACK_NODE)mallock(STACK_NODE_STRUCT_SIZE);
	// Allocate memory

	Stack_Node->data = data;
	Stack_Node->next = NULL;

	return Stack_Node;
}

extern STACK new_Stack()
{
	STACK Stack = (STACK)malloc(STACK_STRUCT_SIZE);
	// Allocate memory
	
	Stack->current = NULL; // No current node
	Stack->size = 0; // Stack are empty

	return Stack;
} 

extern void Stack_push(STACK Stack, _STACK_DATA data)
{
	STACK_NODE Stack_Node = new_Stack_Node(data);
	// We allocate memory for a new element for the stack

	Stack_Node->next = Stack->current;
	Stack->current = Stack_Node;
	// Link a new node to the stack
	
	++Stack->size; // Increase Stack size
}

extern _STACK_DATA Stack_pop(STACK Stack)
{
	_STACK_DATA ext_data = Stack_top(Stack);
	// Extracted data

	STACK_NODE Stack_Node = Stack->current;
	// This node will be deleted
	--Stack->size;
	// Change size of Stack
	
	Stack->current = Stack_Node->next;
	// Change the current Stack Node
	free(Stack_Node);
	// Delete old Stack Node
	
	return ext_data; // Pull data from the stack
}

extern void Stack_makeNull(STACK Stack)
{
	for (int i = 0; i < Stack->size; ++i)
	{

#ifdef _COMPLEX_STACK_DATA		
		Stack_Node_free(Stack->current->data);
#endif		
		
		Stack_pop(Stack);
	}
}
