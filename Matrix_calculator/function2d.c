/*
 * function2d.c
 * 
 * Copyright 2020 Vas Sferd <vassferd@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>

#include "function2d.h"

#define elif else if
// Временное определение

#define Matrix_ifERR(MATRIX_SUSPECT) \
	if (MATRIX_SUSPECT == NULL) \
		return MatrERROR()
// Проверка на ошибку
// В случае ошибки возвращается матрица с одним элементом.


static MATRIX MatrERROR()
{
	MATRIX Matrix = new_Matrix(1, 1);
	GME(Matrix, 0, 0) = -1;
	
	return Matrix;
	// Возвращает матрицу с единственным элементом -1.
}

extern MATRIX new_Matrix(int size_x, int size_y)
{
	MATRIX Matrix = (MATRIX)malloc(MATRIX_STRUCT_SIZE);

#ifdef _USE_2D // Матрица - указатель на массив указателей на столбцы.	

	Matrix->elem = (MET**)malloc(size_x * sizeof(MET*)); //  Создание массива-строки указателей на элементы

	for (int i = 0; i < size_x; ++i)
	{
		Matrix->elem[i] = (MET*)malloc(size_y * sizeof(MET));
	}
	// Выделяем память в столбцах

#else

	Matrix->elem = (MET*)malloc(size_x * size_y * sizeof(MET));
	// Выделяем память сразу для всех элементов матрицы

#endif

	Matrix->size_x = size_x; // Копируем данные о размере в данные структуры
	Matrix->size_y = size_y; // ^

	return Matrix;
}

extern MATRIX Matrix_copy(MATRIX Source_Matrix) // Возвращает указатель на новую матрицу, являющуюся копией исходной.
{
	Matrix_ifERR(Source_Matrix); // Проверка на ошибку источника
	
	MATRIX Matrix = new_Matrix(Source_Matrix->size_x, Source_Matrix->size_y); // Выделяем память под копию
	
	for (int i = 0; i < Source_Matrix->size_x; ++i)
		for (int j = 0; j < Source_Matrix->size_y; ++j)
			GME(Matrix, i, j) = GME(Source_Matrix, i, j);
			// Копируем содержимое
		
	return Matrix;
}

extern MATRIX Matrix_scan(void)
{
	int x, y;
	scanf("%d", &x);
	scanf("%d", &y);
	
	return Matrix_input(x, y);
}

extern MATRIX Matrix_input(int size_x, int size_y)
{
	MATRIX Matrix = new_Matrix(size_x, size_y);
	
	for (int i = 0; i < Matrix->size_x; ++i)
		for (int j = 0; j < Matrix->size_y; ++j)
			scanf("%lf", &GME(Matrix, i, j));
	
	return Matrix;
}

extern MATRIX Matrix_cwrv(int size_x, int size_y)
{
	// Создание матрицы определённого размера со случайными значениями
	
	MATRIX Matrix = new_Matrix(size_x, size_y);
	Matrix_value_generate(Matrix);

	return Matrix;
}

extern MATRIX Matrix_transpose(MATRIX Source_Matrix)
{
	Matrix_ifERR(Source_Matrix); // Проверка на ошибку источника
	
	MATRIX Matrix = new_Matrix(Source_Matrix->size_y, Source_Matrix->size_x);
	// Намеренно создаём матрицу с "перепутанными" размерностями
	
	for (int i = 0; i < Source_Matrix->size_y; ++i)
		for (int j = 0; j < Source_Matrix->size_x; ++j)
			GME(Matrix, i, j) = GME(Source_Matrix, j, i);
			// Теперь заполняем эту матрицу
	
	return Matrix;
}

extern MATRIX Matrix_summ(MATRIX Matrix_1, MATRIX Matrix_2, char mode) // Сумма матриц
{
	Matrix_ifERR(Matrix_1); // Проверка на ошибку источника
	Matrix_ifERR(Matrix_2); // Проверка на ошибку источника
	
	if (Matrix_1->size_x != Matrix_2->size_x || Matrix_1->size_y != Matrix_2->size_y)
	{
		perror("FATAL ERROR: You tried to stack matrices of different sizes\n");
		return NULL;
	}
	
	MATRIX Result_Matrix; // Выводимое значение
	
	if (mode == 'n') // Режим записи в новый объект
		Result_Matrix = new_Matrix(Matrix_1->size_x, Matrix_1->size_y);
	elif (mode == 'r') // Режим записи результата в первую матрицу.
		Result_Matrix = Matrix_1;
	else
	
	{
		perror("FATAL ERROR: Matrix save mode is not correct\n");
		return NULL;
	}
	
	for (int i = 0; i < Matrix_1->size_x; ++i)
		for (int j = 0; j < Matrix_1->size_y; ++j)
			GME(Result_Matrix, i, j) = GME(Matrix_1, i, j) + GME(Matrix_2, i, j);
			// ^ Складываем содержимое матриц
	
	return Result_Matrix;
}

extern MATRIX Matrix_multiply_by_matrix(MATRIX Matrix_1, MATRIX Matrix_2) // Произведение матриц
{
	Matrix_ifERR(Matrix_1); // Проверка на ошибку источника
	Matrix_ifERR(Matrix_2); // Проверка на ошибку источника
	
	if (Matrix_1->size_x != Matrix_2->size_y)
	{
		perror("FATAL ERROR: You tried to multiply matrices that cannot be multiplied\n(Incorrect matrix size)\n");
		return NULL;
	}

	MATRIX Result_Matrix = new_Matrix(Matrix_2->size_x, Matrix_1->size_y);
	// Выделяем память под новую матрицу
	
	for (int i = 0; i < Matrix_2->size_x; ++i)
		for (int j = 0; j < Matrix_1->size_y; ++j)
		{	
			GME(Result_Matrix, i, j) = 0;
			
			for(int k = 0; k < Matrix_1->size_x; ++k)
				GME(Result_Matrix, i, j) += GME(Matrix_1, k, j) * GME(Matrix_2, i, k);
		}
	
	return Result_Matrix;
}

extern int Matrix_multiply_by_num(MATRIX Matrix, double n) // Умножение матрицы на число
{
	for (int i = 0; i < Matrix->size_x; ++i)
		for (int j = 0; j < Matrix->size_y; ++j)
			GME(Matrix, i, j) *= n;
	
	return 0;
}

extern int Matrix_value_generate(MATRIX Matrix) // Генерация случайных значений
{
	srand(RKEY);

	for (int i = 0; i < Matrix->size_x; ++i)
		for (int j = 0; j < Matrix->size_y; ++j)
			GME(Matrix, i, j) = (rand() % rand()) / ((MET)rand() + 1000);
			// Заполняем матрицу случайными значениями
	
	return 0;
}

extern int Matrix_free(MATRIX Matrix)
{
#ifdef _USE_2D
	for (int i = 0; i < size_x; ++i)
	{
		free(Matrix->elem[i]);
	}
	// Освобождаем память в столбцах

	free(Matrix->elem);
	// Удаление массива-строки указателей на элементы

#else
	free(Matrix->elem);
	// Освобождаем память сразу для всех элементов матрицы

#endif
	free(Matrix);

	return 0;
}

extern int Matrix_print(MATRIX Matrix) // Печать матрицы в stdout
{
	for (int i = 0; i < Matrix->size_x; ++i)
	{
		for (int j = 0; j < Matrix->size_y; ++j)
		{
			printf("%8.4lf", GME(Matrix, i, j));
		}

	putchar('\n');
	}

	putchar('\n');

	return 0;
}

#undef elif // Отмена временного определения
