/*
 * matr_calc.c
 * 
 * Copyright 2020 Vas Sferd <vassferd@gmail.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include <stdio.h>
#include "function2d.h"

enum ProgSTATUS
{
	EXIT,
	START
};

int del_Data_and_repeat(MATRIX Matrix_A, MATRIX Matrix_B)
{
	Matrix_free(Matrix_A);
	Matrix_free(Matrix_B);
	
	return START;
}

int program(void)
{
	// Variant 10
	// A * At + k * B
	
	printf("Вычисление выражения \"A * At + k * B\":\n");
	
	printf("Введите размеры матрицы A, а затём её содержимое\n");
	MATRIX Matrix_A = Matrix_scan();
	// Сканируем матрицу A
	
	printf("Введите размеры матрицы B, а затем её содержимое\n");
	MATRIX Matrix_B = Matrix_scan();
	// Создаю матрицу B
	
	printf("Введите число k\n");
	double k;
	scanf("%lf", &k);
	// Ввели число k
	
	printf("Вы ввели:\n");
	// Выводим все полученные данные для их проверки
	
	printf("Матрица A:\n");
	Matrix_print(Matrix_A);
	// Вывожу матрицу A
	
	printf("Матрица B:\n");
	Matrix_print(Matrix_B);
	// Вывожу матрицу B
	
	printf("k = %8.4lf\n", k);
	
	printf
	(
	"\n"
	"Всё верно? Если да, нажмите \"y\" для продолжения\n"
	"Если данные ошибочны то нажмите \"n\"\n"
	"Если вы желаете остановить программу нажмите \"q\"\n"
	":"
	);
	
	char User_choice; // Выбор пользователя
	
	getchar(); // Пропускаем пробельный символ
	
	while (1)
	{	
		User_choice = getchar();
		
		if (User_choice == 'y')
			break;
			// Если да,то продолжается работа с этими данными
		if (User_choice == 'n')
			return del_Data_and_repeat(Matrix_A, Matrix_B);
			// Если нет до память освобождается и программа начинается с начала
		if (User_choice == 'q')
			return EXIT; 
		else
		{	
			printf("Неверный идентификатор. Попробуйте ещё раз.\n:");
		}
	}
	
	printf("Транспонируем матрицу A.\nМатрица At:\n");
	MATRIX Matrix_At = Matrix_transpose(Matrix_A);
	// Транспонирование матрицы A
	Matrix_print(Matrix_At);
	// Вывожу матрицу At (транспонированную)

	printf("Произведение матрицы A и матрицы At:\n");
	MATRIX Matrix_C = Matrix_multiply_by_matrix(Matrix_A, Matrix_At);
	// C = A * At
	Matrix_print(Matrix_C);
	// Вывожу полученную матрицу C
	
	printf("Произведение матрицы B на число k\n");
	Matrix_multiply_by_num(Matrix_B, k);
	// Находим произведение матрицы и числа
	Matrix_print(Matrix_B);
	// Печатаем матрицу B после умножения её членов на k
	
	printf("Итоговая матрица:\n");
	MATRIX Matrix_Result = Matrix_summ(Matrix_C, Matrix_B, 'n');
	// Находим сумму и записываем её в новую матрицу
	Matrix_print(Matrix_Result);
	// Печать результата
	
	fflush(stdin); // Очищаем поток данных
	printf("Запустить программу повторно? (y\\n)\n:");
	
	getchar(); // Пропускаем пробельный символ
	
	while (1)
	{
		User_choice = getchar(); // Выбор пользователя
		
		if (User_choice == 'y')
			return del_Data_and_repeat(Matrix_A, Matrix_B);
			// Повторный запуск программы
		if (User_choice == 'n' || User_choice == 'q')
			return EXIT;
			// Завершение программы
		else
		{	
			printf("Неверный идентификатор. Попробуйте ещё раз.\n:");
		}
	} 
}

int main(void)
{
	while (program());
	// Программа выполняется пока не будет возвращён 0
	
	printf("Программа заверешена...\n");
}
